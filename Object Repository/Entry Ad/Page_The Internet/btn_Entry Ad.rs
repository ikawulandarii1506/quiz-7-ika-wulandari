<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Entry Ad</name>
   <tag></tag>
   <elementGuidId>6ece53c1-e231-43de-939c-f5e8999c058a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/ul/li[15]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>34f3f581-82ed-4204-a49d-b0d78dc5e96c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/entry_ad</value>
      <webElementGuid>b0f3ceb6-8fee-4bb4-a0a7-49d1c4e65ede</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Entry Ad</value>
      <webElementGuid>d211580d-3545-4d90-b7cb-2f760ea03644</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/ul[1]/li[15]/a[1]</value>
      <webElementGuid>f2c76043-f0d1-4bad-8672-b876183d75ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/ul/li[15]/a</value>
      <webElementGuid>09431557-d334-4f43-a65f-9f2176fdfcba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Entry Ad')]</value>
      <webElementGuid>ff26c5dd-b01d-407d-81e4-5bf243c17adb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dynamic Loading'])[1]/following::a[1]</value>
      <webElementGuid>af1b75bd-c6e2-4d69-b907-43441ff8ccf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dynamic Controls'])[1]/following::a[2]</value>
      <webElementGuid>1f6e5db2-7233-4b05-8cad-55b5ba3b53ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exit Intent'])[1]/preceding::a[1]</value>
      <webElementGuid>1bdc56f3-772a-46d5-9a55-dab89b13f31b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File Download'])[1]/preceding::a[2]</value>
      <webElementGuid>6774ae6e-d9ef-474b-9750-327efbfe0b6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Entry Ad']/parent::*</value>
      <webElementGuid>2aa81239-7916-46e1-9d8d-d6688ab3b727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/entry_ad')]</value>
      <webElementGuid>07571cbd-c5db-4403-a1bf-d6832a6e719d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[15]/a</value>
      <webElementGuid>d74007d3-109d-4301-9efa-b41dddd12d6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/entry_ad' and (text() = 'Entry Ad' or . = 'Entry Ad')]</value>
      <webElementGuid>d68d0660-2712-4246-913f-d5dc0d77e063</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

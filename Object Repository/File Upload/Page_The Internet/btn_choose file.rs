<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_choose file</name>
   <tag></tag>
   <elementGuidId>74b899b5-84f8-4c0d-b45d-40f559d2fb7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'file-upload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-upload</value>
      <webElementGuid>6673e7a8-0f12-4afb-b6a2-a18f782360a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>b3880378-8a8c-4da5-a960-232134a447ff</webElementGuid>
   </webElementProperties>
</WebElementEntity>

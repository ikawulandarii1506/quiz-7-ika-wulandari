import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://the-internet.herokuapp.com/')

WebUI.waitForElementPresent(findTestObject('Form Authentication/Page_The Internet/Link_Form Authentication'), 3)

WebUI.click(findTestObject('Form Authentication/Page_The Internet/Link_Form Authentication'))

WebUI.waitForElementPresent(findTestObject('Form Authentication/Page_The Internet/btn_Login'), 3)

WebUI.setText(findTestObject('Form Authentication/Page_The Internet/inp_Username'), 'aaa')

WebUI.setText(findTestObject('Form Authentication/Page_The Internet/inp_Password'), 'aaa')

WebUI.click(findTestObject('Form Authentication/Page_The Internet/btn_Login'))

WebUI.verifyElementPresent(findTestObject('Form Authentication/Page_The Internet/username is invalid'), 5)

WebUI.closeBrowser()

